use std::env;

#[derive(Debug)]
pub enum SignalError {
    NotFound,
    AlreadySent,
}

/// Radio stations are used to manage signals for a specific channel
///
/// # Examples
/// ```
/// use radiowave::*;
///
/// enum Channel {
///     AppEvents = 0,
/// }
///
/// fn main() {
///     let station = RadioStation::new(Channel::AppEvents as usize);
///
///     if station.signal_exists("saycow") == false {
///         println!("Hmmmm... let us say moooooo!");
///
///         station.send_signal("saycow", "Mooooo! Says the cow.").unwrap(); // This statement fails if the signal already exists.
///     }
///
///     else {
///         println!("Cancelling signal!");
///
///         station.cancel_signal_if_exists("saycow");
///     }
/// }
///
/// fn moo() {
///     let station = RadioStation::new(Channel::AppEvents as usize);
///
///     let what_does_the_cow_say: String = station.wait_for_signal("saycow");
///
///     println!("The cow says: {}", what_does_the_cow_say);
/// }
/// ```
pub struct RadioStation {
    channel: String,
}

impl RadioStation {
    /// Create a new radio station
    pub fn new<C: ToString>(channel: C) -> Self {
        return Self {
            channel: channel.to_string(),
        };
    }

    // Private function for getting variable name.
    fn var_name(&self, signal: &str) -> String {
        return format!("__SIGNAL_CHANNEL_{}_SIGNAL_{}__", self.channel, signal);
    }

    /// Broadcast a signal (Fails if already sent)
    pub fn send_signal(&self, signal: &str, data: &str) -> Result<(), SignalError> {
        if self.signal_exists(signal) {
            return Err(SignalError::AlreadySent);
        }

        self.overwrite_signal(signal, data);

        return Ok(());
    }

    /// Broadcast a signal, but if it already is sent, overwrite it
    pub fn overwrite_signal(&self, signal: &str, data: &str) {
        env::set_var(self.var_name(signal), data);
    }

    /// Get the value attached to a signal.
    pub fn get_signal_data(&self, signal: &str) -> Result<String, SignalError> {
        if self.signal_exists(signal) == false {
            return Err(SignalError::NotFound);
        }

        let data: String = env::var(self.var_name(signal).as_str()).unwrap();

        return Ok(data);
    }

    /// Pause the code until the signal has been received (also cancels the signal when received)
    pub fn wait_for_signal(&self, signal: &str) -> String {
        loop {
            if self.signal_exists(signal) {
                let data: String = self.get_signal_data(signal).unwrap();

                self.cancel_signal_if_exists(signal);

                return data;
            }
        }
    }

    /// Check to see if a signal is waiting to be listened to
    pub fn signal_exists(&self, signal: &str) -> bool {
        return match env::var(self.var_name(signal)) {
            Ok(_) => true,
            Err(_) => false,
        };
    }

    /// Cancel a signal
    pub fn cancel_signal(&self, signal: &str) -> Result<(), SignalError> {
        if self.signal_exists(signal) {
            env::remove_var(self.var_name(signal));

            return Ok(());
        }

        else {
            return Err(SignalError::NotFound);
        }
    }

    /// Cancel a signal if it exists
    pub fn cancel_signal_if_exists(&self, signal: &str) {
        if self.signal_exists(signal) {
            self.cancel_signal(signal).unwrap();
        }
    }
}
