# RadioWave

A Rust crate for emitting signals across the entire program.

# Examples

```rust
use radiowave::*;

enum Channel {
    AppEvents = 0,
}

fn main() {
    let station = RadioStation::new(Channel::AppEvents as usize);

    if station.signal_exists("saycow") == false {
        println!("Hmmmm... let us say moooooo!");

        station.send_signal("saycow", "Mooooo! Says the cow.").unwrap(); // This statement fails if the signal already exists.
    }

    else {
        println!("Cancelling signal!");

        station.cancel_signal_if_exists("saycow");
    }
}

fn moo() {
    let station = RadioStation::new(Channel::AppEvents as usize);

    let what_does_the_cow_say: String = station.wait_for_signal("saycow");

    println!("The cow says: {}", what_does_the_cow_say);
}
```
